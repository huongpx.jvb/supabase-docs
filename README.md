# Supabase Docs
Hướng dẫn tạo Supabase project để nhanh chóng có 1 server REST APIs.
Các ví dụ sử dụng Vue 3 để demo.

## Mục lục:
- [Đăng kí tài khoản](https://gitlab.com/huongpx.jvb/supabase-docs/-/wikis/%C4%90%C4%83ng-k%C3%AD-t%C3%A0i-kho%E1%BA%A3n)
- [Tạo project](https://gitlab.com/huongpx.jvb/supabase-docs/-/wikis/T%E1%BA%A1o-project)
- [Tạo bảng dữ liệu](https://gitlab.com/huongpx.jvb/supabase-docs/-/wikis/T%E1%BA%A1o-b%E1%BA%A3ng-d%E1%BB%AF-li%E1%BB%87u)
- [Thao tác cơ bản với APIs]()
- [Bật RLS (row level security) để hạn chế truy cập]()
- [Tạo policy để phân quyền truy cập]()
- [Gọi API từ frontend]()
    - [Đăng kí tài khoản]()
    - [Đăng nhập]()
    - [Làm việc với APIs]()
